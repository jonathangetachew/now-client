# now-client
EthioNow is a platform for viewing currently available movies in selected cinemas across Addis Ababa.
Made with 
    [x] Vuejs CLI 3
    [x] Firebase Firestore
    [x] VuexFire
    [x] Leaflet
    [x] Vue2leaflet

npm install @vue/cli firebase vuexfire@next leaflet vue2leaflet

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
