import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import PageNotFound from './views/PageNotFound.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior() {
    // Scroll to top on each route loaded
    return {
      x: 0,
      y: 0,
    };
  },
  routes: [
    {
      path: '*',
      name: 'pagenotfound',
      component: PageNotFound,
    },
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        title: 'Home - EthioNow',
        metaTags: [
          {
            name: 'description',
            content: 'Get information and schedules for currently showing and upcoming movies in Cinemas around Addis Ababa, Ethiopia.',
          },
          {
            property: 'og:description',
            content: 'The home page of EthioNow.',
          },
        ],
      },
    },
    {
      path: '/movies/:id',
      name: 'movie',
      component: () => import('./views/Movie.vue'),
      meta: {
        title: 'Movie Page - EthioNow',
        metaTags: [
          {
            name: 'description',
            content: 'Movie Schedules page of EthioNow.',
          },
          {
            property: 'og:description',
            content: 'See nearby cinemas on a Map.',
          },
        ],
      },
    },
    {
      path: '/admin-dashboard-login',
      name: 'login',
      component: () => import('./views/AdminPortal.vue'),
      meta: {
        title: 'Admin Portal - EthioNow',
        metaTags: [
          {
            name: 'description',
            content: 'Login page for admins of EthioNow.',
          },
        ],
      },
    },
    {
      path: '/admin-dashboard',
      name: 'dashboard',
      component: () => import('./views/Dashboard.vue'),
      meta: {
        title: 'Dashboard - EthioNow',
      },
    },
  ],
});

router.beforeEach((to, from, next) => {
  // This goes through the matched routes from last to first,
  // finding the closest route with a title.
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
  const previousNearestWithMeta = from.matched.slice().reverse()
    .find(r => r.meta && r.meta.metaTags);

  // If a route with a title was found, set the document (page) title to that value.
  if (nearestWithTitle) document.title = nearestWithTitle.meta.title;

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll('[data-vue-router-controlled]'))
    .map(el => el.parentNode.removeChild(el));

  // Skip rendering meta tags if there are none.
  if (!nearestWithMeta) return next();

  // Turn the meta tag definitions into actual elements in the head.
  nearestWithMeta.meta.metaTags.map((tagDef) => {
    const tag = document.createElement('meta');

    Object.keys(tagDef).forEach((key) => {
      tag.setAttribute(key, tagDef[key]);
    });

    // We use this to track which meta tags we create, so we don't interfere with other ones.
    tag.setAttribute('data-vue-router-controlled', '');

    return tag;
  }).forEach(tag => document.head.appendChild(tag)); // Add the meta tags to the document head.

  return next();
});

export default router;
