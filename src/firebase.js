import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

// Initialize Firebase
const config = {
  apiKey: 'AIzaSyAaqM3NNBdn8gYTWnO5lZg3GXUgFubUdNI',
  authDomain: 'enow-c4dd2.firebaseapp.com',
  databaseURL: 'https://enow-c4dd2.firebaseio.com',
  projectId: 'enow-c4dd2',
  storageBucket: 'enow-c4dd2.appspot.com',
  messagingSenderId: '384319111943',
};

firebase.initializeApp(config);

export default firebase;
