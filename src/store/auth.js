import firebase from '@/firebase';
import router from '@/router';

const state = {
  user: {},
  isLoggedIn: false,
};

const mutations = {
  setUser(state, user) {
    if (user) {
      state.user = user;
      state.isLoggedIn = true;
    } else {
      state.user = {};
      state.isLoggedIn = false;
    }
  },
};

const actions = {
  loginWithEmailPass(_, loginInfo) {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .signInWithEmailAndPassword(loginInfo.email, loginInfo.password)
        .then(
          (response) => {
            // Let the calling function know that the request is done. You may send some data back
            resolve(response);
          },
          (error) => {
            reject(error);
          },
        );
    });
  },

  signUpWithEmailPass(_, loginInfo) {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .createUserWithEmailAndPassword(loginInfo.email, loginInfo.password)
        .then(
          (response) => {
            const { user } = response;

            if (user) {
              user.updateProfile({
                displayName: loginInfo.displayName,
              });
            }
            resolve(response);
          },
          (error) => {
            reject(error);
          },
        );
    });
  },

  async loginWithGoogle() {
    const provider = new firebase.auth.GoogleAuthProvider();
    await firebase.auth().signInWithPopup(provider).then(() => {
      router.push('/admin-dashboard');

      // Redirect user to where they came from
      // if (window.history.length > 2) {
      //   router.go(-1);
      // } else {
      //   router.push('/');
      // }
    });
  },

  async logout() {
    await firebase.auth().signOut();
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
