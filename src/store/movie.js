import { firebaseAction } from 'vuexfire';
import db from '@/db';

const state = {
  movies: [],
};

const getters = {
  // eslint-disable-next-line
  movie: state => (state.movies[0] ? state.movies[0] : {}),
};

const actions = {
  initMovie: firebaseAction(({ bindFirebaseRef }, id) => {
    // Bind firebase data with state
    bindFirebaseRef('movies', db.collection('movies').where('id', '==', id));
  }),
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
};
