import Vue from 'vue';
import Vuex from 'vuex';
import { firebaseMutations } from 'vuexfire';

import auth from './auth';
import movies from './movies';
import movie from './movie';

Vue.use(Vuex);

export default new Vuex.Store({
  mutations: {
    ...firebaseMutations,
  },
  modules: {
    auth,
    movies,
    movie,
  },
});
