import { firebaseAction } from 'vuexfire';
import db from '@/db';

const movies = db.collection('movies');

const state = {
  all_movies: [],
  nowShowingMovies: [],
  comingSoonMovies: [],
};

const actions = {
  initMovies: firebaseAction(({
    bindFirebaseRef,
  }) => {
    // Bind firebase data with state
    bindFirebaseRef(
      'all_movies',
      movies.orderBy('status', 'desc').orderBy('title').orderBy('language', 'desc').limit(20),
    );
  }),
  initNowShowing: firebaseAction(({
    bindFirebaseRef,
  }) => {
    // Bind firebase data with state
    bindFirebaseRef(
      'nowShowingMovies',
      movies.where('status', '==', 'Now Showing').orderBy('title').orderBy('language', 'desc').limit(12),
    );
  }),
  initComingSoon: firebaseAction(({
    bindFirebaseRef,
  }) => {
    // Bind firebase data with state
    bindFirebaseRef(
      'comingSoonMovies',
      movies.where('status', '==', 'Coming Soon').orderBy('title').orderBy('language', 'desc').limit(5),
    );
  }),
};

export default {
  namespaced: true,
  state,
  actions,
};
